# Basic config
TESTING_MODE = True

# Telegram config
TG_SESSION = 'evwatcher'
TG_API_ID = ''
TG_API_HASH = ''
TG_WATCH_GROUPS = ('Satoshistreasure',)
TG_WATCH_USERS = ('wheatpond',)

# Discord config
DISCORD_WEBHOOK_URL = ''
