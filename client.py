import os, sys, time, logging
from telethon import TelegramClient, events, sync, utils
from telethon.tl.functions.channels import JoinChannelRequest
from dhooks import Webhook
from diskcache import Cache

logging.basicConfig(level=logging.ERROR)
from config import *

cache = Cache('%s.cache' % TG_SESSION)
client = TelegramClient(TG_SESSION, TG_API_ID, TG_API_HASH)
hook = Webhook(DISCORD_WEBHOOK_URL)


def send_msg(channel, message):
    if message.reply_to_msg_id:
        reply_to = client.get_messages(channel, ids=message.reply_to_msg_id)
        if reply_to:
            reply_to_user = client.get_entity(client.get_input_entity(reply_to.from_id))
            msg = "[%s] >> %s: %s" % (reply_to.date.isoformat(), utils.get_display_name(reply_to_user), reply_to.message)
            print(msg)
            if not TESTING_MODE:
                hook.send(msg)
    from_user = client.get_entity(client.get_input_entity(message.from_id))
    msg = "[%s] %s: %s" % (message.date.isoformat(), utils.get_display_name(from_user), message.message)
    print(msg)
    if not TESTING_MODE:
        hook.send(msg)


async def async_send_msg(channel, message):
    if message.reply_to_msg_id:
        reply_to = await client.get_messages(channel, ids=message.reply_to_msg_id)
        if reply_to:
            reply_to_entity = await client.get_input_entity(reply_to.from_id)
            reply_to_user = await client.get_entity(reply_to_entity)
            msg = "[%s] >> %s: %s" % (reply_to.date.isoformat(), utils.get_display_name(reply_to_user), reply_to.message)
            print(msg)
            if not TESTING_MODE:
                hook.send(msg)
    from_entity = await client.get_input_entity(message.from_id)
    from_user = await client.get_entity(from_entity)
    msg = "[%s] %s: %s" % (message.date.isoformat(), utils.get_display_name(from_user), message.message)
    print(msg)
    if not TESTING_MODE:
        hook.send(msg)


@client.on(events.NewMessage(chats=TG_WATCH_GROUPS, from_users=TG_WATCH_USERS))
async def my_handler(event):
    channel = await event.get_chat()
    await async_send_msg(channel, event.message)
    group = utils.get_display_name(channel)
    cachekey = '%s|min_id' % (group)
    cache.set('%s|min_id' % (group), event.message.id)
    if TESTING_MODE:
        print("Set cache key %s = %s" % (cachekey, event.message.id))


client.start()

dialogs = client.get_dialogs()
joined = [ dialog.name for dialog in dialogs ]

# Parse missed messages
for group in TG_WATCH_GROUPS:
    channel = client.get_entity(group.lower())
    if not group in joined:
        client(JoinChannelRequest(channel))
    for user in TG_WATCH_USERS:
        for message in client.iter_messages(channel, from_user=user, reverse=True, min_id=cache.get('%s|min_id' % (group), default=0)):
            send_msg(channel, message)
            cache.set('%s|min_id' % (group), message.id)


client.run_until_disconnected()
